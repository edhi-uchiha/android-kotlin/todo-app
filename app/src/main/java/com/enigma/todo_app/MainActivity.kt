package com.enigma.todo_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.enigma.todo_app.auth.SignInActivity
import com.enigma.todo_app.utils.AppPreference

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val preference = AppPreference().getCredentials(this)

        if(preference !== ""){
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }
    }

    fun onLoginButtonClicked(view: View) {
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }
}