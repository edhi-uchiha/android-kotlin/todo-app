package com.enigma.todo_app.auth

import androidx.lifecycle.MutableLiveData
import com.enigma.todo_app.dto.ResponseBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class AuthRepository @Inject constructor  (private val authService: AuthService) {

    var profile: MutableLiveData<Users> = MutableLiveData<Users>()

    suspend fun login(payload: UserLoginDto): Response<ResponseBuilder> {
        return withContext(Dispatchers.IO){
            authService.login(payload)
        }
    }

    suspend fun getProfile(): Response<ResponseBuilder> {
        return withContext(Dispatchers.IO) {
            authService.getProfile()
        }
    }
}