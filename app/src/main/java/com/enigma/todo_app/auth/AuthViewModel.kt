package com.enigma.todo_app.auth

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.ViewModel
import com.enigma.todo_app.dto.Resource
import com.enigma.todo_app.dto.ResponseBuilder
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import java.lang.Exception
import javax.inject.Inject

class AuthViewModel @Inject constructor (private var authRepository: AuthRepository) : ViewModel(){

    private val gson: Gson = GsonBuilder().create()
    var profile: MutableLiveData<Users> = authRepository.profile

    fun login(userLoginDto: UserLoginDto) = liveData(Dispatchers.IO) {
        try {
            emit(Resource.loading(data = null))
            val resp = authRepository.login(userLoginDto)
            if(resp.code() == 201){
                val respData = ResponseBuilder(status = resp.body()!!.status, data = resp.body()!!.data)
                emit(Resource.success(data = respData))
            }
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message!!))
        }
    }

    fun profile() = liveData(Dispatchers.Main) {
        try {
            val resp = authRepository.getProfile()
            if(resp.code() == 200){
                val respBody = resp.body()!!
                val respData = ResponseBuilder(status = respBody.status, respBody.data)
                profile.value = gson.fromJson(gson.toJson(respBody.data), Users::class.java)
                emit(Resource.success(data = respData))
            }
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message!!))
        }
    }

}