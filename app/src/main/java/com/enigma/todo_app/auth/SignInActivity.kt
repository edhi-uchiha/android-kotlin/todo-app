package com.enigma.todo_app.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.enigma.todo_app.HomeActivity
import com.enigma.todo_app.MainActivity
import com.enigma.todo_app.R
import com.enigma.todo_app.config.dagger.TodoApplication
import com.enigma.todo_app.enums.StatusEnum
import com.enigma.todo_app.utils.AppPreference
import com.enigma.todo_app.utils.UIEventManager
import javax.inject.Inject

class SignInActivity : AppCompatActivity(), UIEventManager  {

    @Inject lateinit var authViewModel: AuthViewModel
    @Inject lateinit var preference: AppPreference

    private lateinit var etUsername: EditText
    private lateinit var etPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        (applicationContext as TodoApplication).applicationComponent.inject(this)

        etUsername = findViewById(R.id.et_username)
        etPassword = findViewById(R.id.et_password)

    }

    fun onBackButtonClicked(view: View) {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    fun signIn(view: View) {
        val payload = UserLoginDto(email = etUsername.text.toString(), password = etPassword.text.toString())
        authViewModel.login(payload).observe( this, {
            when(it.status){
                StatusEnum.SUCCESS -> {
                    showToast("Login Success")
                    preference.saveCredential(it.data!!.data, this)
                    startActivity(Intent(this, HomeActivity::class.java))
                    finish()
                }
                StatusEnum.ERROR -> showToast("Invalid login credentials")
                else -> showToast("Loading. . .")
            }
        })
    }

    override fun showToast(message: String) {
       Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    override fun showProgressBar() {
        TODO("Not yet implemented")
    }

    override fun hideProgressBar() {
        TODO("Not yet implemented")
    }
}