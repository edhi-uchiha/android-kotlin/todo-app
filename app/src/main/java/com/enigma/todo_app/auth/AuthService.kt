package com.enigma.todo_app.auth

import com.enigma.todo_app.dto.ResponseBuilder
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthService {

    @POST("api/v1/auth/login")
    suspend fun login(@Body userLoginDto: UserLoginDto) : Response<ResponseBuilder>

    @GET("api/v1/users/profile")
    suspend fun getProfile() : Response<ResponseBuilder>
}