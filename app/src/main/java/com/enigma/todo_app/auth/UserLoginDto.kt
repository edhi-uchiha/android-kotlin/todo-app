package com.enigma.todo_app.auth

data class UserLoginDto(
        val email: String,
        val password: String
)