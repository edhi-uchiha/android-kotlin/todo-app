package com.enigma.todo_app.auth

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Users (

        @SerializedName("id")
        val id: String,

        @SerializedName("fullname")
        val fullname: String,

        @SerializedName("email")
        val email: String,

        @SerializedName("username")
        val username: String,

        @SerializedName("password")
        val password: String,

        @SerializedName("status")
        val status: String,

        @SerializedName("phone")
        val phone: String,

        @SerializedName("lastLogin")
        val lastLogin: String,

        @SerializedName("createdAt")
        val createdAt: String,

        @SerializedName("updatedAt")
        val updatedAt: String
): Parcelable {
    override fun toString(): String {
        return "Users(id='$id', name='$fullname', email='$email', username='$username', password='$password', status='$status', phone='$phone', lastLogin='$lastLogin')"
    }
}