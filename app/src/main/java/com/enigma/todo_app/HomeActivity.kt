package com.enigma.todo_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.enigma.todo_app.auth.AuthViewModel
import com.enigma.todo_app.auth.SignInActivity
import com.enigma.todo_app.auth.Users
import com.enigma.todo_app.config.dagger.TodoApplication
import com.enigma.todo_app.enums.StatusEnum
import com.enigma.todo_app.utils.AppPreference
import javax.inject.Inject

class HomeActivity : AppCompatActivity() {

    @Inject lateinit var authViewModel: AuthViewModel
    @Inject lateinit var preference: AppPreference

    private lateinit var tvUsername: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        (applicationContext as TodoApplication).applicationComponent.inject(this)

        tvUsername = findViewById(R.id.tv_username)
        loadData()
    }

    fun logOut(view: View) {
        preference.removeCredentials(this)
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }

    private fun loadData(){
        authViewModel.profile().observe( this, {
            when(it.status) {
                StatusEnum.SUCCESS -> {
                    val user: Users? = authViewModel.profile.value
                    if (user != null) {
                        Log.d("Profile", user.toString())
                        tvUsername.text = user.fullname
                    }
                }
                else -> Log.d("Profile", "ERROR when loadData()")
            }
        })
    }

    fun refresh(view: View) {
        loadData()
    }
}