package com.enigma.todo_app.dto

import com.google.gson.annotations.SerializedName

data class Status(
    @SerializedName("code")
    var code: String,

    @SerializedName("description")
    var description: String
)

data class ResponseBuilder(
    @SerializedName("status")
    var status: Status,

    @SerializedName("data")
    var data: Any
){
    override fun toString(): String {
        return "ResponseBuilder(status=$status, data=$data)"
    }
}