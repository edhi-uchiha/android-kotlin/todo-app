package com.enigma.todo_app.dto

import com.enigma.todo_app.enums.StatusEnum
import com.enigma.todo_app.enums.StatusEnum.ERROR
import com.enigma.todo_app.enums.StatusEnum.LOADING
import com.enigma.todo_app.enums.StatusEnum.SUCCESS

data class Resource<out T>(val status: StatusEnum, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): Resource<T> = Resource(status = SUCCESS, data = data, message = null)

        fun <T> error(data: T?, message: String): Resource<T> =
            Resource(status = ERROR, data = data, message = message)

        fun <T> loading(data: T?): Resource<T> = Resource(status = LOADING, data = data, message = null)
    }
}