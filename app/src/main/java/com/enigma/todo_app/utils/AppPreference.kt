package com.enigma.todo_app.utils

import android.app.Activity
import android.content.Context
import dagger.Module
import javax.inject.Inject

@Module
class AppPreference @Inject constructor() {

    fun saveCredential(user: Any?, context: Context){
        val editor = context.getSharedPreferences(Companion.USER_CREDENTIALS_KEY, Activity.MODE_PRIVATE).edit()
        editor.putString("credentials", user.toString())
        editor.apply()
    }

    fun getCredentials(context: Context): String? {
        val preference = context.getSharedPreferences(Companion.USER_CREDENTIALS_KEY, Activity.MODE_PRIVATE)
        return preference.getString("credentials", "")
    }

    fun removeCredentials(context: Context){
        val editor = context.getSharedPreferences(Companion.USER_CREDENTIALS_KEY, Activity.MODE_PRIVATE).edit()
        editor.putString("credentials", "")
        editor.apply()
    }

    companion object {
        private const val USER_CREDENTIALS_KEY = "user_credentials"
    }
}