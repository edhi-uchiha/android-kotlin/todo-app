package com.enigma.todo_app.utils

interface UIEventManager {

    fun showToast(message: String)
    fun showProgressBar()
    fun hideProgressBar()

}