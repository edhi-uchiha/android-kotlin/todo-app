package com.enigma.todo_app.config.dagger

import com.enigma.todo_app.HomeActivity
import com.enigma.todo_app.auth.SignInActivity
import com.enigma.todo_app.utils.AppPreference
import dagger.Component

@Component(modules = [NetworkModule::class])
interface ApplicationComponent {

    fun inject(signInActivity: SignInActivity)
    fun inject(homeActivity: HomeActivity)
    fun inject(appPreference: AppPreference)

}