package com.enigma.todo_app.config.dagger

import android.app.Application

class TodoApplication: Application() {

    val applicationComponent: ApplicationComponent = DaggerApplicationComponent.create()
}