package com.enigma.todo_app.config.dagger

import com.enigma.todo_app.auth.AuthService
import com.enigma.todo_app.config.retrofit.RetrofitClient
import com.enigma.todo_app.utils.AppPreference
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {

    @Provides
    fun provideAuthService(): AuthService{
        return RetrofitClient.createRetrofit().create(AuthService::class.java)
    }

    @Provides
    fun provideAppPreference() : AppPreference {
        return AppPreference()
    }

}