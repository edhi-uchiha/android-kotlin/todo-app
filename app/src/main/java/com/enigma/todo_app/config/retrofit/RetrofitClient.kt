package com.enigma.todo_app.config.retrofit

import android.annotation.SuppressLint
import android.util.Log
import android.webkit.CookieManager
import okhttp3.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*


class RetrofitClient {

    companion object {
        private const val BASE_URL = "https://10.0.2.2:3030/"

        fun createRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getUnsafeOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }

        private fun getUnsafeOkHttpClient(): OkHttpClient {
            return try {

                val dispatcher = Dispatcher()
                dispatcher.maxRequests = 1

                // Create a trust manager that does not validate certificate chains
                val trustAllCerts: Array<TrustManager> = arrayOf<TrustManager>(
                        object : X509TrustManager {
                            override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {
                                TODO("Not yet implemented")
                            }

                            @SuppressLint("TrustAllX509TrustManager")
                            override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {

                            }

                            override fun getAcceptedIssuers(): Array<X509Certificate> {
                                return arrayOf<X509Certificate>()
                            }

                        }
                )
                val hostnameVerifier: HostnameVerifier = HostnameVerifier { p0, _ ->
                    Log.d("TAG", "Trust Host :$p0")
                    true
                }

                // Install the all-trusting trust manager
                val sslContext: SSLContext = SSLContext.getInstance("TLS")
                sslContext.init(null, trustAllCerts, SecureRandom())
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory
                OkHttpClient.Builder()
                        .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
                        .dispatcher(dispatcher)
                        .cookieJar(UvCookieJar())
                        .hostnameVerifier(hostnameVerifier).build()
            } catch (e: Exception) {
                throw RuntimeException(e)
            }
        }
    }

}

private class UvCookieJar : CookieJar {

    lateinit var cookieManager: CookieManager

    override fun saveFromResponse(url: HttpUrl, cookies: List<Cookie>) {
        cookieManager = CookieManager.getInstance()
        for (cookie in cookies) {
            cookieManager.setCookie(url.toString(), cookie.toString())
            Log.e("Cookie","saveFromResponse :  Cookie url : " + url.toString() + cookie.toString())
        }
    }

    override fun loadForRequest(url: HttpUrl): List<Cookie> {

        val cookieManager = CookieManager.getInstance()
        val cookies: ArrayList<Cookie> = ArrayList()
        if (cookieManager.getCookie(url.toString()) != null) {
            val splitCookies = cookieManager.getCookie(url.toString()).split("[,;]".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (i in splitCookies.indices) {
                cookies.add(Cookie.parse(url, splitCookies[i].trim { it <= ' ' })!!)
                Log.e("Cookie","loadForRequest :Cookie.add ::  " + Cookie.parse(url, splitCookies[i].trim { it <= ' ' })!!)
            }
        }
        return cookies
    }
}