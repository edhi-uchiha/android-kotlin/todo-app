package com.enigma.todo_app.enums

enum class StatusEnum {
    SUCCESS,
    ERROR,
    LOADING
}